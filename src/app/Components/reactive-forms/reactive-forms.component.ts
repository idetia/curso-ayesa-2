import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Employee } from '../../Model/employee';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.css']
})
export class ReactiveFormsComponent implements OnInit {

  employee: Employee;
  employeeForm: FormGroup;
  jobs: Array<string>;
  
  constructor() {
    this.employee = new Employee('', '');
    this.jobs = ['Project Manager', 'Programmer', 'Designer'];
  }

  ngOnInit() {
    this.employeeForm = new FormGroup({
      name: new FormControl(this.employee.name, [
        Validators.required,
        Validators.minLength(4)
      ]),
      email: new FormControl(this.employee.email, [
        Validators.required,
        Validators.minLength(4),
        this.forbiddenEmailValidator
      ]),
      job: new FormControl(this.employee.job),     
      isActive: new FormControl(this.employee.isActive)           
    });

    this.employeeForm.valueChanges.subscribe(
        // Se ha producido un cambio
        value => {

        if (!this.employeeForm.valid) {
          return;
        }          

        console.log(value);
        this.employee = new Employee(
          value.name,
          value.email,
          value.job,
          value.age,
          value.isActive

        );
      },
      // Un error
      () => {
        console.log('Algún error...');
      }
    );
  }

  forbiddenEmailValidator(email: FormControl) {
    // Emails prohibidos
    let forbiddenEmails: Array<string> = ['dev1@gmail', 'dev2@gmail'];

    if (forbiddenEmails.indexOf(email.value) != -1) {
      // El email no es válido
      return {
        'invalid': true
      };
    }

    // El email es válido
    return null;
  }


}
