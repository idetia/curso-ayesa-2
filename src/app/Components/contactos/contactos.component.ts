import { Component, OnInit, OnDestroy } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { Contact } from '../../Model/contact';
import { AgendService } from '../../Services/agend.service';
import { Observable } from 'rxjs/Observable';

const END_POINT = 'https://curso-angular-ayesa.firebaseio.com/Contactos';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.css']
})
export class ContactosComponent implements OnInit, OnDestroy {

  contactForm: FormGroup;
  contact: Contact;
  contacts: Array<Contact>;
  updateSubscription?: Subscription;
  suscriptionContacts?: Subscription;

  constructor(private httpClient: HttpClient,
    private agendService: AgendService) {
    this.contact = new Contact('', '');
    this.updateSubscription = null;
    this.contacts = [];
  }

  ngOnDestroy() {
    // Nos desuscribimos al salir del componente
    if (this.suscriptionContacts && !this.suscriptionContacts.closed) {
      this.suscriptionContacts.unsubscribe();
    }
  }

  ngOnInit() {
    // Instaciamos un nuevo formulario
    this.contactForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required])
    });

    this.contactForm.valueChanges.subscribe(value => {
      if (this.contactForm.valid) {
        this.contact = new Contact(
          this.contactForm.controls['name'].value,
          this.contactForm.controls['phone'].value,
          this.contact.id
        );

        // Si estamos editando
        if (this.contact.id != null) {
          this.editContacto();
        }
      }
    });

    this.getContactos();
  }

  // Nuevo contacto
  nuevoContacto(){

    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    let options = {headers: headers};
    let body = JSON.stringify(this.contact);
  
    this.httpClient.post(END_POINT + '.json', body, options).subscribe(
      // Callback cambio
      response => {

        this.getContactos();

        // Resetea formulario
        this.contact = new Contact();
        this.contactForm.reset();
      },

      // Callback error
      error => console.log('Se ha producido un error ' + error),

      // Finaliza observable
      () => console.log('Fin petición')
    );
  }

  deleteContact(contact: Contact, button){

    button.disabled = true;
    this.httpClient.delete(END_POINT + '/' + contact.id + '.json')
    .subscribe(
      // Callback de éxito
      response => {
        this.getContactos();
        console.log(`El contacto con ID ${contact.id} se borró. `);
      },

      // Callback de error
      error => {
        console.log(`El contacto con ID ${contact.id} NO se borró.`);
        button.disabled = false;
      }

    );
  }

  // Editar contacto
  editContacto(){

    // Si hay una suscripción previa y no está cerrada, la cancelamos
    // antes de volver a suscribirnos
    if (this.updateSubscription != null && !this.updateSubscription.closed) {
      this.updateSubscription.unsubscribe();
    }

    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    let options = {headers: headers};
    let body = JSON.stringify(this.contact);
  
    this.updateSubscription = this.httpClient.put(END_POINT + '/' + this.contact.id + '.json', body, options).subscribe(
      // Callback cambio
      response => {

        this.getContactos();
      },

      // Callback error
      error => console.log('Se ha producido un error ' + error),

      // Finaliza observable
      () => console.log('Fin petición')
    );
  }

  // Listado de contactos
  getContactos() {
    let contacts = this.agendService.getContactos();

    // Si recibimos un Observable
    if (contacts instanceof Observable) {
      this.suscriptionContacts = contacts.subscribe(
        // Ok
        response => {
          this.contacts = response;

          this.agendService.getContactByName('Estebann').subscribe(
            // Hubo un cambio (coincidió el nombre)
            contacto => {
              if (contacto) {
              console.log("Se encontró el contacto " + contacto.name);
              } else {
                console.log("Todavía no lo ha encontrado");
              }
            },

            // Error
            error => console.log(error)
          );
        }, 

        // Error
        error =>  console.log(error)
      );
    
    // Si ya nos llega el array
    } else {
      this.contacts = contacts;
    }
  }

  // Carga el contacto para ser editado
  loadContact(contact: Contact) {
    this.contact = contact;
    this.contactForm.controls['name'].setValue(contact.name);
    this.contactForm.controls['phone'].setValue(contact.phone);    

    console.log(this.contact);
  }


}
