import { Component, OnInit } from '@angular/core';
import { AgendService } from '../../Services/agend.service';
import { Contact } from '../../Model/contact';

@Component({
  selector: 'app-listado-contactos',
  templateUrl: './listado-contactos.component.html',
  styleUrls: ['./listado-contactos.component.css']
})
export class ListadoContactosComponent implements OnInit {

  contacts: Array<Contact>;

  constructor(private agendService: AgendService) {
    this.contacts = this.agendService.getContacts();
  }

  ngOnInit() {
  }

}
