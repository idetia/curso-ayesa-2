import { Component, OnInit, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Child2Component } from '../child2/child2.component';
import { SimpleMessageDirective } from '../../Directives/simple-message.directive';
import { ChildComponent } from '../child/child.component';
import { Contact } from '../../Model/contact';
import { AgendService } from '../../Services/agend.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  parentVariable: string = "Variable del padre";
  childComponentVar: string;
  childComponentVar2: number;
  contact: Contact = new Contact('', '');

  @ViewChild(Child2Component) child: Child2Component;
  @ViewChildren(Child2Component) children: QueryList<Child2Component>;
  @ViewChild('mensaje') mensaje: ElementRef;
  @ViewChild('unaDirectiva', {read: SimpleMessageDirective}) mensaje2: SimpleMessageDirective;

  @ViewChild(ChildComponent) eChild: ChildComponent;

  sumaTodos() {
    this.children.forEach(child => {
      child.suma();
    });
  }

  alertMessage: any;

  constructor(private agendService: AgendService) { }

  ngOnInit() {
    this.mensaje.nativeElement.style.color = "white";
    this.mensaje.nativeElement.style.backgroundColor = "black";
    this.mensaje.nativeElement.innerHTML = "Un mensaje";  
    
    this.mensaje2.texto = "Se ha cambiado el texto de esta directiva";

  }

  getChildVar(evento: string) {
    this.childComponentVar = evento;
  }

  getChildVar2(evento: number) {
    this.childComponentVar2 = evento;
  }

  changeMessage(type) {
    switch(type) {
      case 'info':
        this.alertMessage = {
          type: type,
          message: 'Mensaje de información'
        };
        break;
      case 'error':
        this.alertMessage = {
          type: type,
          message: 'Mensaje de error'
        };  
        break;      
      case 'warning':
        this.alertMessage = {
          type: type,
          message: 'Mensaje de aviso'
        };  
        break;              
    }
  }

  // Agrega un nuevo contacto
  nuevoContacto(formulario) {
    this.agendService.addContact(this.contact);

    //Resetea
    this.contact = new Contact('','');
    formulario.form.reset();
  }
}
