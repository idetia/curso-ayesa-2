import { Component, OnInit } from '@angular/core';


import { DatePipe } from '@angular/common';
import { StripHtmlPipe } from '../../Pipes/strip-html.pipe';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css'],
  providers: [DatePipe, StripHtmlPipe]
})
export class PipesComponent implements OnInit {

  birthdate: Date = new Date(1995, 3, 20); // 20 de abril de 1995

  dummyText: string = "Lorem ipsum dolor sit amet.";

  htmlText: string = 'Texto <strong>con etiquetas</strong> <u>HTML</u>';

  constructor(private datePipe: DatePipe, private stripHtmlPipe: StripHtmlPipe) {
    // Strip HTML
    console.log(this.stripHtmlPipe.transform(this.htmlText));

    // Date Pipe
    console.log(this.datePipe.transform(this.birthdate, 'dd-MM-yyyy'));
   }

  ngOnInit() {
    
  }

}
