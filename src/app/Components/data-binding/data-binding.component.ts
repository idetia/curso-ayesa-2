import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  ejemplo1: string = "Ejemplo de data-binding por interpolación";
  ejemplo2: any = {
    name: "Ricardo Fernández",
    email: "ricfer@gmail.com"
  };
  
  // Ejemplo event binding
  counter: number = 0;
  sumaAction(): void{
    this.counter++;
  }

  // Ejemplo two-way data binding
  content: string = "Escribe aquí tu nombre...";

  constructor() { }

  ngOnInit() {
  }

}
