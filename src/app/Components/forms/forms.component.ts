import { Component, OnInit } from '@angular/core';

import { Employee } from '../../Model/employee';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  employee: Employee;
  jobs: Array<string>;

  constructor() {
    this.employee = new Employee('', '');
    this.jobs = ['Project Manager', 'Programmer', 'Designer'];
  }

  ngOnInit() {
  }

  envioFormulario(formulario: any):void {
    console.log(this.employee);

    formulario.form.reset();
  }

}
