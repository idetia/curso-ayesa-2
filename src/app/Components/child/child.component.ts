import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ParentComponent } from '../parent/parent.component';
import { DummyEmployeeService } from '../../Services/dummy-employee.service';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit, OnChanges {

  @Input() parentComponentVar: string;
  @Output() childEvent:EventEmitter<string> = new EventEmitter();
  @Output() childEvent2:EventEmitter<number> = new EventEmitter();
  texto: string = "Un texto ...";
  numero: number = 123;

  constructor(private dummyEmployeeService: DummyEmployeeService) { }

  ngOnInit() {
    this.childEvent.emit(this.texto);
    this.childEvent2.emit(this.numero);
  }

  ngOnChanges(changes: SimpleChanges ) {
    if (changes) {
      console.log(changes);
      this.parentComponentVar = changes.parentComponentVar.currentValue;
    }
  }

  disparaEventos() {
    this.childEvent.emit("Nuevo valor XXX");
    this.childEvent2.emit(987654321);
  }
}
