import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { DummyEmployeeService } from '../../Services/dummy-employee.service';

@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.css']
})
export class RoutingComponent implements OnInit {

  employees: Array<any>;
  activeEmployee: any;
  params: any;

  constructor(
    private router: Router,
    private activedRoute: ActivatedRoute,
    private dummyEmployee: DummyEmployeeService) {
    this.employees = dummyEmployee.getEmployees();
  }

  ngOnInit() {
    this.params = this.activedRoute.params.subscribe(params => {
      console.log('Entra al suscriptor');
      
      // Busca el empleado por el id que nos llega de la ruta
      this.activeEmployee = this.employees.find(x => x.id == +params['id']);
    });
  }

  showEmployee(id: number) {
    // Navega hacia una ruta
    this.router.navigate(['rutas', id]);
  }
}
