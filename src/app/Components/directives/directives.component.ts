import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  // Ejemplo para ngFor
  colores: Array<string> = ['Azul', 'Verde', 'Amarillo', 'Rojo'];

  // Ejemplo para ngIf
  colorActivo: string;
  cambiaColor(color: string): void{
    this.colorActivo = color;
  }

  // Ejemplo para ngTemplate
  userIsLogin: boolean = false;
  changeUserStatus(): void {
    this.userIsLogin = !this.userIsLogin;
  }

  // Ejemplo para ngClass
  alertStatus: string;
  changeAlertStatus(status: string): void{
    this.alertStatus = status;
  }

  // Ejemplo para ngStyle
  alertStatus2: string;
  changeAlertStatus2(status: string): void{
    this.alertStatus2 = status;
  }
  printInlineStyles(): any {
    let fgColor : string = "#000";
    let bgColor : string = "#fff";
    
    switch(this.alertStatus2) {
      case 'success':
        fgColor = "#fff";
        bgColor = "green";      
        break;
      case 'error':
        fgColor = "#fff";
        bgColor = "red";              
        break;
    }

    return {'background-color': bgColor, 'color': fgColor};
    
  };

  constructor() { }

  ngOnInit() {
  }

}
