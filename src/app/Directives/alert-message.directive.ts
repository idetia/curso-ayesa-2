import { Directive, ElementRef, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Direct } from 'protractor/built/driverProviders';

@Directive({
  selector: '[appAlertMessage]'
})
export class AlertMessageDirective implements OnInit, OnChanges {

  @Input() appAlertMessage?: any;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.doAction();
  }

  doAction() {
    if (this.appAlertMessage == null || this.appAlertMessage == undefined) {
      return;
    }

    let classes: string = "alert";

    switch(this.appAlertMessage.type) {
      case 'info':
        classes += ' alert-info';
        break;
      case 'warning':
        classes += ' alert-warning';
        break;
      case 'error':
        classes += ' alert-danger';     
        break;           
    }

    this.el.nativeElement.setAttribute('class', classes);
    this.el.nativeElement.innerHTML = this.appAlertMessage.message;
  }

  ngOnChanges(changes: SimpleChanges ) {
    if (changes) {
      console.log(changes);
      this.appAlertMessage = changes.appAlertMessage.currentValue;
      this.doAction();
    }
  }
}
