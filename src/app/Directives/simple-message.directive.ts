import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appSimpleMessage]'
})
export class SimpleMessageDirective implements OnInit {

  texto: string = 'Mi primera directiva Angular';
  
  constructor(private el: ElementRef) { }

  ngOnInit() {
    // Agrega el atributo class con las clases que queramos al elemento
    // que contiene la directiva
    this.el.nativeElement.setAttribute('class', 'alert alert-info');

    // Agrega un texto dentro del elemento que contiene la directiva
    this.el.nativeElement.innerHTML = this.texto;
  }

}
