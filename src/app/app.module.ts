import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import localeEsExtra from '@angular/common/locales/extra/es';

import { APP_ROUTING } from './app.routing';

import { AppComponent } from './app.component';
import { BodyComponent } from './Components/body/body.component';
import { HeaderComponent } from './Components/header/header.component';
import { DataBindingComponent } from './Components/data-binding/data-binding.component';
import { DirectivesComponent } from './Components/directives/directives.component';
import { SimpleMessageDirective } from './Directives/simple-message.directive';
import { PipesComponent } from './Components/pipes/pipes.component';
import { StripHtmlPipe } from './Pipes/strip-html.pipe';
import { FormsComponent } from './Components/forms/forms.component';
import { ReactiveFormsComponent } from './Components/reactive-forms/reactive-forms.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { RoutingComponent } from './Components/routing/routing.component';

import { DummyEmployeeService } from './Services/dummy-employee.service';
import { RoutingChildComponent } from './Components/routing-child/routing-child.component';
import { ParentComponent } from './Components/parent/parent.component';
import { ChildComponent } from './Components/child/child.component';
import { AlertMessageDirective } from './Directives/alert-message.directive';
import { Child2Component } from './Components/child2/child2.component';
import { RestaComponent } from './Components/resta/resta.component';
import { ListadoContactosComponent } from './Components/listado-contactos/listado-contactos.component';
import { AgendService } from './Services/agend.service';
import { LoggerService } from './Services/logger.service';
import { ContactosComponent } from './Components/contactos/contactos.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    HeaderComponent,
    DataBindingComponent,
    DirectivesComponent,
    SimpleMessageDirective,
    PipesComponent,
    StripHtmlPipe,
    FormsComponent,
    ReactiveFormsComponent,
    PageNotFoundComponent,
    RoutingComponent,
    RoutingChildComponent,
    ParentComponent,
    ChildComponent,
    AlertMessageDirective,
    Child2Component,
    RestaComponent,
    ListadoContactosComponent,
    ContactosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    APP_ROUTING,
    HttpClientModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'es-ES'},
    DummyEmployeeService,
    AgendService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

registerLocaleData(localeEs, 'es-ES', localeEsExtra);