import { Routes, RouterModule } from '@angular/router';
import { DataBindingComponent } from './Components/data-binding/data-binding.component';
import { DirectivesComponent } from './Components/directives/directives.component';
import { PipesComponent } from './Components/pipes/pipes.component';
import { FormsComponent } from './Components/forms/forms.component';
import { ReactiveFormsComponent } from './Components/reactive-forms/reactive-forms.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { RoutingComponent } from './Components/routing/routing.component';
import { RoutingChildComponent } from './Components/routing-child/routing-child.component';
import { ParentComponent } from './Components/parent/parent.component';
import { ContactosComponent } from './Components/contactos/contactos.component';

export const APP_ROUTES: Routes = [
    {path: '', pathMatch: 'full', redirectTo: '/data-binding'},
    {path: 'data-binding', component: DataBindingComponent},
    {path: 'directivas', component: DirectivesComponent},
    {path: 'filtros', component: PipesComponent, outlet: 'menu2'},
    {path: 'formularios', component: FormsComponent},
    {path: 'formularios-reactivos', component: ReactiveFormsComponent},
    {path: 'rutas', component: RoutingComponent},
    {path: 'rutas/:id', component: RoutingComponent, children: [
        {path: 'editar', component: RoutingChildComponent}
    ]},
    {path: 'comunicacion', component: ParentComponent},
    {path: 'contactos', component: ContactosComponent},    
    {path: '**', component: PageNotFoundComponent}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);