import { Contact } from '../Model/contact';
import { Injectable, Optional } from '@angular/core';
import { LoggerService } from './logger.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/map';

const END_POINT = 'https://curso-angular-ayesa.firebaseio.com/Contactos';

@Injectable()
export class AgendService {

  contacts: Array<Contact> = [];
  subject = new Subject<Contact>();

  constructor(@Optional() private logger: LoggerService,
  private httpClient: HttpClient) { }

  getContactByName(name: string): Observable<Contact> {
    let index = 0;

    // Proceso asíncrono, llama cada 5 segundo a la función
    let interval = setInterval(() => {
      let contact: Contact = (this.contacts[index] && this.contacts[index].name == name) ?
      this.contacts[index]:null;
      index++;

      if (contact) {
        this.subject.next(contact);
        this.subject.complete();

        clearInterval(interval);
      } else {
        this.subject.next();
        console.log(`No se encuentra en la posicón ${index}`);
      }

      if (index == this.contacts.length && !this.subject.isStopped) {
        this.subject.error("La búsqueda finalizó y no se encontró.");
        clearInterval(interval);
      }

    }, 5000);

    // Se devuelve el observable
    return this.subject.asObservable();
  }

  addContact(contact: Contact) {
    if (this.logger) {
      this.logger.log('Añadido nuevo contacto');
    }
    this.contacts.push(contact);
  }



  // Listado de contactos
  getContactos(refresh: boolean = false): Observable<Contact[]> | Array<Contact> {

    // Si ya hay contactos cargados lo devolvemos
    if (!refresh && this.contacts.length > 0) {
      return this.contacts;
    }

    return this.httpClient.get(END_POINT + '.json').map(
      // Éxito
      response => {

        this.contacts = [];

        // Recorremos el objeto devuelto para darle el formato adecuado
        for (let key in response) {
          this.contacts.push(new Contact(
            response[key].name,
            response[key].phone,
            key
          ));
        }

        return this.contacts;

      }
    );

  }
  


  getContacts() {
    return this.contacts;
  }
}
