export class LoggerService {

  constructor() { }

  log(msg: string) {
    console.log('DEBUG\n' + msg);
  }

  error(msg: string) {
    console.error('ERROR\n' + msg);
  }

  warn(msg: string) {
    console.error('WARNING\n' + msg);
  }
}
