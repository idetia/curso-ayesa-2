import { TestBed, inject } from '@angular/core/testing';

import { DummyEmployeeService } from './dummy-employee.service';

describe('DummyEmployeeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DummyEmployeeService]
    });
  });

  it('should be created', inject([DummyEmployeeService], (service: DummyEmployeeService) => {
    expect(service).toBeTruthy();
  }));
});
