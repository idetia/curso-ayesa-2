export class DummyEmployeeService {

  constructor() { }

  getEmployees(): Array<any> {
    return [
      {
        id: 1,
        name: 'Sergio Rodríguez',
        email: 'serrod@gmail.com'
      },
      {
        id: 2,
        name: 'Ángela Pérez',
        email: 'angper@gmail.com'
      },
      {
        id: 3,
        name: 'Rafael Fernández',
        email: 'raffer@gmail.com'
      }       
    ];
  }
}
